package com.klymchuk.model;

import com.klymchuk.model.stateImp.CodeReviewState;
import com.klymchuk.model.stateImp.DoneState;
import com.klymchuk.model.stateImp.InProcessingState;
import com.klymchuk.model.stateImp.ToDoState;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<Task> tasks = new ArrayList<>();
    private int indexOfTask;

    public void addTask(String taskName){
        indexOfTask = tasks.size();
        tasks.add(new Task(taskName));
    }

    public void changeIndexOfTask(int number){
        indexOfTask = number-1;
    }

    public void setTaskState(int numberOfState){
        Task task = tasks.get(indexOfTask);
        switch (numberOfState) {
            case 1:
                task.getState().toDo(task);
                break;
            case 2:
                task.getState().inProgress(task);
                break;
            case 3:
                task.getState().CodeReview(task);
                break;
            case 4:
                task.getState().done(task);
                break;
        }
    }

    public String getTasks(){
        StringBuilder sb = new StringBuilder("\n");
        for (Task t:tasks){
            sb.append(t);
        }
        sb.append("\n");
        return sb.toString();
    }

    @Override
    public String toString() {
        return "tasks number: " + (indexOfTask+1) +
                "\t" + tasks.get(indexOfTask) +
                "\n";
    }
}
