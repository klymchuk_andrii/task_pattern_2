package com.klymchuk.model.stateImp;

import com.klymchuk.model.State;
import com.klymchuk.model.Task;

public class InProcessingState implements State {
    @Override
    public void CodeReview(Task task) {
        task.setState(new CodeReviewState());
    }

    @Override
    public void toDo(Task task) {
        task.setState(new ToDoState());
    }

    @Override
    public String toString() {
        return "InProcessingState";
    }
}
