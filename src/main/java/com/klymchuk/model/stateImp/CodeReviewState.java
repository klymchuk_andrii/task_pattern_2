package com.klymchuk.model.stateImp;

import com.klymchuk.model.State;
import com.klymchuk.model.Task;

public class CodeReviewState implements State {
    @Override
    public void done(Task task) {
        task.setState(new DoneState());
    }

    @Override
    public void inProgress(Task task) {
        task.setState(new InProcessingState());
    }

    @Override
    public void toDo(Task task) {
        task.setState(new ToDoState());
    }

    @Override
    public String toString() {
        return "CodeReviewState";
    }
}
