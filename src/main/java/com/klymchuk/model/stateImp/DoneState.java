package com.klymchuk.model.stateImp;

import com.klymchuk.model.State;

public class DoneState implements State {
    @Override
    public String toString() {
        return "DoneState";
    }
}
