package com.klymchuk.model.stateImp;

import com.klymchuk.model.State;
import com.klymchuk.model.Task;

public class ToDoState implements State {
    @Override
    public void inProgress(Task task) {
        task.setState(new InProcessingState());
    }

    @Override
    public String toString() {
        return "ToDoState";
    }
}
