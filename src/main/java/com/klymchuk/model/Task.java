package com.klymchuk.model;

import com.klymchuk.model.stateImp.ToDoState;

public class Task {
    private static int counter = 0;
    private State state;
    private String taskName;
    private int taskNumber;

    public Task(String tskName) {
        this.state = new ToDoState();
        this.taskName = tskName;
        this.taskNumber = counter++;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return taskNumber +
                " taskName:'" + taskName +
                "  state:" + state;
    }
}
