package com.klymchuk.controller;

import com.klymchuk.model.Model;

public class Controller {
    private Model model;

    public Controller(){
        model = new Model();
    }

    public void choiceTask(int number){
        model.changeIndexOfTask(number);
    }

    public void addTask(String taskName){
        model.addTask(taskName);
    }

    public String getInfo(){
        return model.toString();
    }

    public void setTaskState(int numberOfState){
        model.setTaskState(numberOfState);
    }

    public String getTasks(){
        return model.getTasks();
    }


}
