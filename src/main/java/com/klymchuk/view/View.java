package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private Controller controller;

    private Logger logger;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public View() {
        controller = new Controller();
        logger = LogManager.getLogger(View.class);

        input = new Scanner(System.in);

        setMenu();

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showTasks);
        methodsMenu.put("2", this::addTask);
        methodsMenu.put("3", this::getInfo);
        methodsMenu.put("4",this::choiceTask);
        methodsMenu.put("5",this::setState);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show tasks");
        menu.put("2", "2 - add task");
        menu.put("3", "3 - get info about current task");
        menu.put("4", "4 - choice task, enter task number");
        menu.put("5", "5 - set state, enter state number");
    }

    private void Menu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            Menu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }


    public void showTasks(){
        logger.info(controller.getTasks());
    }

    public void getInfo(){
        logger.info(controller.getInfo());
    }

    public void choiceTask(){
        controller.choiceTask(input.nextInt());
        input.nextLine();
    }

    public void addTask(){
        controller.addTask(input.nextLine());
    }

    public void setState(){
        controller.setTaskState(input.nextInt());
        input.nextLine();
    }
}